#define DEBUG 1

#define RELAY_FLUSH 13
#define RELAY_FILL  12
#define INDICATOR_LED LED_BUILTIN
#define ACTIVATE_BUTTON 3

#define PRESSURE_GAGUE A0 //Pressure gague wired to analog read port A0

//Gague paramaters as cpp macros to allow them to be optimized away where possible,
//because I don't know how smart the Arduino compiler is
#define MAX_PRESSURE 1000 //units of torr
#define MIN_PRESSURE 0    //units of torr
#define MAX_GAGUE_RAW 1023     //units of 
#define MIN_GAGUE_RAW 0     //units of V

#define GAGUE_M (MAX_PRESSURE - MIN_PRESSURE) / (MAX_GAGUE_RAW - MIN_GAGUE_RAW)
#define GAGUE_B MAX_PRESSURE - (GAGUE_M * MAX_GAGUE_RAW)



#define FF_CYCLE_COUNT 3

#define FF_PRESSURE_LOW 10
#define FF_PRESSURE_HIGH 700

volatile int pressure = 0;

void setup() {
  volatile int v;
  
  pinMode(RELAY_FLUSH, OUTPUT);
  pinMode(RELAY_FILL, OUTPUT);
  pinMode(INDICATOR_LED, OUTPUT);

  pinMode(ACTIVATE_BUTTON, INPUT_PULLUP);
  attachInterrupt(ACTIVATE_BUTTON, ff_cycle, RISING);

  pinMode(PRESSURE_GAGUE, INPUT);
  

  #if DEBUG
    Serial.begin(9600);
  #endif

  //ADC stabalize
  for (byte i=0; i<10; i++) {
    v = analogRead(PRESSURE_GAGUE);
  }

  

}

void loop() {
  #if DEBUG
    Serial.print(read_pressure());
    Serial.print("\n");
    delay(1000);
  #endif

}

float read_pressure() {
  int raw = analogRead(PRESSURE_GAGUE);
  return GAGUE_M * raw + GAGUE_B;
}

void ff_cycle() {
  #if DEBUG
    Serial.print("--- Starting Flush-Fill cycle ---\n");
  #endif
  //Verify correct relay initial relay states:
  digitalWrite(RELAY_FLUSH, LOW);
  digitalWrite(RELAY_FILL,  LOW);
  
  for(int i=0; i<FF_CYCLE_COUNT; i++) {
    digitalWrite(RELAY_FILL,  LOW);
    digitalWrite(RELAY_FLUSH, HIGH); //begin flush
    while(read_pressure() > FF_PRESSURE_LOW) {
      delay(100);
    }
    digitalWrite(RELAY_FLUSH, LOW);
    digitalWrite(RELAY_FILL,  HIGH);
    while(read_pressure() < FF_PRESSURE_HIGH) {
      delay(100);
    }
  }

  #if DEBUG
    Serial.print("---End Flush-Fill cycle ---\n");
  #endif
}

